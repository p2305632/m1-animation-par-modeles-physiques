/*
 * CalculsMSS.cpp :
 * Copyright (C) 2016 Florence Zara, LIRIS
 *               florence.zara@liris.univ-lyon1.fr
 *               http://liris.cnrs.fr/florence.zara/
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** \file CalculsMSS.cpp
Programme calculant pour chaque particule i d un MSS son etat au pas de temps suivant 
 (methode d 'Euler semi-implicite) : principales fonctions de calculs.
\brief Fonctions de calculs de la methode semi-implicite sur un systeme masses-ressorts.
*/ 

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>

#include "vec.h"
#include "ObjetSimule.h"
#include "ObjetSimuleMSS.h"
#include "Viewer.h"

using namespace std;





/**
* Calcul des forces appliquees sur les particules du systeme masses-ressorts.
 */
void ObjetSimuleMSS::CalculForceSpring()
{
	/// f = somme_i (ki * (l(i,j)-l_0(i,j)) * uij ) + (nuij * (vi - vj) . uij)
	
	/// Rq : Les forces dues a la gravite et au vent sont ajoutees lors du calcul de l acceleration

	for(int i = 0; i < _SystemeMasseRessort->GetNbParticule(); i++)
	{
		// f est le vecteur de force de la particule i.
		Vector f = Vector();
		Particule* p = _SystemeMasseRessort->GetParticule(i);
		std::vector<Ressort *>& ressorts = p->GetRessortList();

		for(unsigned int j = 0; j < ressorts.size(); j++)
		{
			Ressort* r = ressorts[j];
			float k = r->GetRaideur();
			float nu = r->GetAmortissement();
			float l0 = r->GetLrepos();
			Vector OA = P[r->GetParticuleA()->GetId()];
			Vector OB = P[r->GetParticuleB()->GetId()];

			Vector vA = V[r->GetParticuleA()->GetId()];
			Vector vB = V[r->GetParticuleB()->GetId()];

			// On veut A = particule courante, B autre particule reliée par le ressort
			if(r->GetParticuleA()->GetId() != i)
			{
				std::swap(OA, OB);
				std::swap(vA, vB);
			}

			Vector AB = OB - OA;
			float l = length(AB);
			Vector u = normalize(AB);

			//if (false)
			if (l > 10.f)
			{
				r->GetSpring()->_Amorti = 0.f;
				r->GetSpring()->_Nu = 0.f;
				r->GetSpring()->_Raideur = 0.f;
			}
			else
				f = f + ( k * (l - l0) * u + nu * dot((vB - vA), u) * u );

			if (length(f) > 100000.f)
			{
				r->GetSpring()->_Amorti = 0.f;
				r->GetSpring()->_Nu = 0.f;
				r->GetSpring()->_Raideur = 0.f;
			}
		}


		Force[i] = f;
	}
    
		
}//void


/**
 * Gestion des collisions avec le sol.
 */
void ObjetSimuleMSS::Collision()
{
    /// Arret de la vitesse quand touche le plan

	for (int i = 0; i < P.size(); i++)
	{
		const auto& pos = P[i];

		const float positionSol = -8.f;
		if (pos.y < positionSol)
		{
			V[i].y = 0.f; // En y on arrête la vitesse.
			// Pour le plan du sol, on ajoute un peu de friction pour que le tissu accroche au sol.
			V[i].x *= 0.9f;
			V[i].z *= 0.9f;
		}
	}
    
}// void

